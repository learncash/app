import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Vue from 'vue-native-core'

// importing modules
import { VueNativeBase } from "native-base";
Vue.use(VueNativeBase);

// vuex
// import store from './store/store';

// Root File, App.vue
import App from './App.vue'

export default App
// export default function App() {
//   return (AppVue);
//   // return (
//   //   <View style={styles.container}>
//   //     <Text>Hello!</Text>
//   //   </View>
//   // );
// }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
