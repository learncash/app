export default {
  methods: {
    handlerScreen(screen) {
      this.navigation.navigate(screen);
    },
    Logout() {
      this.navigation.navigate("Login");
    }
  }
}